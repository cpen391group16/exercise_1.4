/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "WiFi.h"
#include "lcd_touch.h"

#define PUSH_BUTTONS ((~(*(volatile unsigned char *)(0x80002060)))& 0x7)

// Hacks to print binary
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

char wifiBuffer[500];
int wifiMsgReady = 0;
int wifiMsgIndex = 0;

unsigned char last_push_buttons;

int main()
{
	printf("Hello from Nios II!\n");
	printf("Init_Wifi\n");
	init_wifi();
	printf("Init_lcd_touch\n");
	Init_Touch();

	while(1)
	{
		char buttons = PUSH_BUTTONS;

		if((buttons & 0x1) == 1 && (last_push_buttons != (buttons & 0x1)))
		{
			sendstring_wifi("get_tix");
			printf("Getting Tickets\n");
		}

		if(is_data_ready_wifi())
		{
			wifiBuffer[wifiMsgIndex] = getchar_wifi();
			if(wifiBuffer[wifiMsgIndex] == '\r'){
				wifiMsgReady = 1;
				wifiBuffer[wifiMsgIndex] = '\0';
			}
			else{
				wifiMsgIndex++;
			}
		}

		// wifiMsg has finished
		if(wifiMsgReady){
			printf("WiFi:%s\n",wifiBuffer);
			wifiMsgReady = 0;
			wifiMsgIndex = 0;
		}

		Point press = GetPress();
		printf("Pressed at (%d, %d)\n", press.x, press.y);
		Point release = GetRelease();
		printf("Released at (%d, %d)\n", release.x, release.y);

		last_push_buttons = (buttons & 0x1);
	}

  return 0;
}
