/*
 * lcd_touch.c

 *
 *  Created on: Jan 25, 2018
 *      Author: Mathew
 */

#include <stdio.h>
#include "lcd_touch.h"

#define LCD_TOUCH_Control (*(volatile unsigned char *)(0x84000230))
#define LCD_TOUCH_Status (*(volatile unsigned char *)(0x84000230))
#define LCD_TOUCH_TxData (*(volatile unsigned char *)(0x84000232))
#define LCD_TOUCH_RxData (*(volatile unsigned char *)(0x84000232))
#define LCD_TOUCH_Baud (*(volatile unsigned char *)(0x84000234))

#define X_PIXELS 1024
#define Y_PIXELS 600
#define RESOLUTION 4096

void Init_Touch(void)
{
	// set up 6850 Control Register to utilise a divide by 16 clock,
	// set RTS low, use 8 bits of data, no parity, 1 stop bit,
	// transmitter interrupt disabled
	LCD_TOUCH_Control = 0x15;

	// program baud rate generator to use 9600 baud
	// The loew 3 bits must not be 001, 010, 011, 100
	LCD_TOUCH_Baud = 0x05;

	// Send the byte command to enable touch
	LCD_TOUCH_TxData = 0x12;
}

char getbyte_lcd_touch( void )
{
	// Poll RX bit in 6850 status register and await for it to become '1'
	while (!is_data_ready_touch()) {};

	// Read the received character from 6850 RxData register.
	return LCD_TOUCH_RxData & 0xff;
}

// the following function polls the 6850 to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read
int is_data_ready_touch(void)
{
	// Test Rx bit in 6850 serial comms chip status register
	// if RX bit is set, return TRUE, otherwise return FALSE
	return 0x01 & LCD_TOUCH_Status;
}

int ScreenTouched(void) {
	return is_data_ready_touch();
}

int ScreenPressed(void) {
	if(is_data_ready_touch()) {
		char data = getbyte_lcd_touch();
		if((data & 0xff) == 0x81) {
			return 1;
		}
	}
	return 0;
}

int ScreenReleased(void) {
	if(is_data_ready_touch()) {
		char data = getbyte_lcd_touch();
		if((data & 0xff) == 0x80) {
			return 1;
		}
	}
	return 0;
}

void WaitForTouch() {
	while(!ScreenTouched()) {}
}

void WaitForPress() {
	while(!ScreenPressed()) {}
}

void WaitForRelease() {
	while(!ScreenReleased()) {}
}

Point GetPress(void) {
	Point p1;
	WaitForPress();

	char b2 = getbyte_lcd_touch();
	char b3 = getbyte_lcd_touch();
	char b4 = getbyte_lcd_touch();
	char b5 = getbyte_lcd_touch();

	// Extract the coordinates
	p1.x = (int)((int)(b3 & 0b00011111) << 7) | (int)(b2 & 0b01111111);
	p1.y = (int)((int)(b5 & 0b00011111) << 7) | (int)(b4 & 0b01111111);

	// Map the coordinates to pixel values
	p1.x = (int) (X_PIXELS * p1.x / RESOLUTION);
	p1.y = (int) (Y_PIXELS * p1.y / RESOLUTION);

	return p1;
}

Point GetRelease(void) {
	Point p1;
	WaitForRelease();

	char b2 = getbyte_lcd_touch();
	char b3 = getbyte_lcd_touch();
	char b4 = getbyte_lcd_touch();
	char b5 = getbyte_lcd_touch();

	// Extract the coordinates
	p1.x = (int)((int)(b3 & 0b00011111) << 7) | (int)(b2 & 0b01111111);
	p1.y = (int)((int)(b5 & 0b00011111) << 7) | (int)(b4 & 0b01111111);

	// Map the coordinates to pixel values
	p1.x = (int) (X_PIXELS * p1.x / RESOLUTION);
	p1.y = (int) (Y_PIXELS * p1.y / RESOLUTION);

	return p1;
}
