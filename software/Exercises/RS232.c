/*
 * RS232.c
 *
 *  Created on: Jan 20, 2018
 *      Author: timot
 */
#import "RS232.h"


#define RS232_Control (*(volatile unsigned char *)(0x84000200))
#define RS232_Status (*(volatile unsigned char *)(0x84000200))
#define RS232_TxData (*(volatile unsigned char *)(0x84000202))
#define RS232_RxData (*(volatile unsigned char *)(0x84000202))
#define RS232_Baud (*(volatile unsigned char *)(0x84000204))

/**************************************************************************
/* Subroutine to initialise the RS232 Port by writing some data
** to the internal registers.
** Call this function at the start of the program before you attempt
** to read or write to data via the RS232 port
**
** Refer to 6850 data sheet for details of registers and
***************************************************************************/

void Init_RS232(void)
{
	// set up 6850 Control Register to utilise a divide by 16 clock,
	// set RTS low, use 8 bits of data, no parity, 1 stop bit,
	// transmitter interrupt disabled
	RS232_Control = 0x15;

	// program baud rate generator to use 115k baud
	RS232_Baud = 0x01;

}
int sendcharRS232(int c)
{
	// Poll Tx bit in 6850 status register and await for it to become '1'
	while (!(0x02 & RS232_Status)) {};

	// Write the character to the 6850 TxData register.
	RS232_TxData = c;

	return c;
}
int getcharRS232( void )
{
	// Poll RX bit in 6850 status register and await for it to become '1'
	while (!(0x01 & RS232_Status)) {};

	// Read the received character from 6850 RxData register.
	return (int) RS232_RxData;
}

// the following function polls the 6850 to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read
int RS232TestForReceivedData(void)
{
	// Test Rx bit in 6850 serial comms chip status register
	// if RX bit is set, return TRUE, otherwise return FALSE
	return 0x01 & RS232_Status;
}
